public class Rectanle extends Figure{
    public double a;
    public double b;

    public Rectanle(){};
    public Rectanle(String type, double a, double b) {
        super(type);
        this.a=a;
        this.b=b;
        this.type=type;
    };

    @Override
    public Double S(Double a, Double b) {
        a=a*b;
        return a;
    }

    @Override
    public Double P(Double a, Double b, double c) {
        a=(a+b)*2;
        return a;
    }

    @Override
    public void Print() {
        System.out.println("Тип: "+type+"\nШирина: "+Math.min(a,b)+"\nДлина: "+Math.max(a,b)+"\nПериметр: "+P(a,b,0)+"\nПлощадь: "+S(a,b)+"\n");
    }
}
