public class Triangle extends Figure{
    public double a;
    public double b;
    public  double c;

    public Triangle(){};
    public Triangle(String type, double a, double b, double c) {
        super(type);
        this.a=a;
        this.b=b;
        this.c=c;
        this.type=type;
    };

    @Override
    public Double P(Double a, Double b, double c) {
        a=a+b+c;
        return a;
    }

    public Double S(Double a, Double b,Double c) {
        a=a*H(a,b,c);
        return a;
    }

    public Double H(Double a, Double b, Double c){
        double p= P(a,b,c)/2;
        double h= 2*Math.sqrt(p*(p-a)*(p-b)*(p-c))/a;
        return h;
    }
    @Override
    public void Print() {
        System.out.println("Тип: "+type+"\n1-ая сторона: "+a+"\n2-ая сторона: "+b+"\n3-я сторона: "+c+"\nПериметр: "+P(a,b,c)+"\nПлощадь: "+S(a,b,c)+"\n");
    }
}
