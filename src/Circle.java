public class Circle extends  Figure{
    public double a;

    public Circle(){};
    public Circle(String type, double a) {
        super(type);
        this.a=a;
        this.type=type;
    };

    @Override
    public Double S(Double a, Double b) {
        a=a*a*Math.PI;
        return a;
    }

    @Override
    public Double P(Double a, Double b, double c) {
        a=2*Math.PI*a;
        return a;
    }

    @Override
    public void Print() {
        System.out.println("Тип: "+type+"\nРадиус: "+a+"\nПериметр: "+P(a,0.0,0)+"\nПлощадь: "+S(a,0.0)+"\n");
    }
}
